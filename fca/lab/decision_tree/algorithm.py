from .utils import fruits, IG
from fca import Context, Concept


def BuildTree():

    posCtx, negCtx, undCtx = fruits()
    attrs = posCtx.attributes
    globalCtx = Context([], attrs, [])
    for index in range(len(posCtx)): 
        globalCtx.add_object(posCtx.objects[index], posCtx[index])
    for index in range(len(negCtx)): 
        globalCtx.add_object(negCtx.objects[index], negCtx[index])
    globalCtx = globalCtx.dichotomize()


    def computeOptimalBranch(parent, globalCtx):

        if len(globalCtx.objects) == 0:
            return
        if len(globalCtx.attributes) == 0:
            return

        res = []
        for m in globalCtx.attributes:
            res.append((IG(globalCtx, posCtx, negCtx, m, "!" + str(m), set()), m))

        res = sorted(res, key=lambda x: x[0])

        #Берем наиболее информативную ветку
        if res[-1][1][0] == "!":
            branch = res[-1][1].replace("!", "")
            negbranch = res[-1][1]
        else: 
            negbranch = "!" + res[-1][1]
            branch = res[-1][1]
        

        def analyseBranch(branch, negbranch):
            objs = globalCtx.derivation_attributes(Concept(set(),set([branch])))
            status = "" 
            if (len(set(objs).difference(posCtx.objects)) == 0):
                status = "+"
            if (len(set(objs).difference(negCtx.objects)) == 0):
                status = "-"
            print(set(globalCtx.objects), "( ) -----", branch, "----> ", objs, "(", status, ")")    
            if status == "":
                attrs = globalCtx.attributes.copy()
                attrs.remove(branch)
                attrs.remove(negbranch)

                return globalCtx.extract_subcontext_by_objects(list(objs)).extract_subcontext_by_attributes(list(attrs))
            return Context([],[],[])

        computeOptimalBranch(branch,analyseBranch(branch, negbranch))
        computeOptimalBranch(negbranch,analyseBranch(negbranch, branch))

    computeOptimalBranch("root", globalCtx)