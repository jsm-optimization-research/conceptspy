from fca import Context, Concept
import math

def fruits():
    attrs = ['w','y','g','b','f','s','r','o']

    posTable = [
        [0,1,0,0,0,1,1,1],
        [0,1,0,0,0,0,1,1],
        [0,0,1,0,0,0,0,1],
        [0,0,0,1,0,1,0,1],
    ]

    negTable = [
        [0,0,1,0,1,1,0,0],
        [1,0,0,0,1,1,0,1],
        [1,0,0,0,0,0,1,1]
    ]

    undTable = [
        [1,1,0,1,1,0,1,0],
    ]

    posCtx = Context(['1','2','3','4'], attrs, posTable)
    negCtx = Context(['5','6','7'], attrs, negTable)
    undCtx = Context(['8'], attrs, undTable)
    return posCtx, negCtx, undCtx

def p(globalCtx, epsCtx, A):
    AA = globalCtx.derivation_attributes(Concept([],A))
    if len(set(AA)) == 0:
        return 0
    # print(AA, A)
    return len(set(AA).intersection(set(epsCtx.objects))) \
        / len(set(AA))
        
def Ent(globalCtx, posCtx, negCtx, A):
    posp = p(globalCtx, posCtx, A)
    negp = p(globalCtx, negCtx, A)


    return -posp*Log2(posp)\
           -negp*Log2(negp)

def Log2(p):
    if p == 0:
        return -100000
    else:
        return math.log2(p)

def IG(globalCtx, posCtx, negCtx, mPos, mNeg, A):
    A_mpos = A.union(set([mPos]))
    A_mneg = A.union(set([mNeg]))

    return - len(A_mpos) / len(globalCtx.objects) * Ent(globalCtx, posCtx, negCtx, A_mpos) \
           - len(A_mneg) / len(globalCtx.objects) * Ent(globalCtx, posCtx, negCtx, A_mneg)