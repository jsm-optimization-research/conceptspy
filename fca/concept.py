# from fuzz import FuzzySet

class Concept:
    """A formal concept (Формальное понятние), contains intent and extent"""

    def __init__(self, extent, intent):
        """Initialize a concept with given extent(объем) and intent(содержание)"""
        self.extent = set(extent)
        self.intent = set(intent)
    
    def __repr__(self):
        """Return a string representation of a concept"""
        return "({0}, {1})\n".format(self.extent, self.intent)



