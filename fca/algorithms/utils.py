from itertools import chain, combinations

def powerset(iterable):
    """All subset of set"""
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

def equalArrayOfConcepts(a, b):
    """"Comparing two arrays of Concepts"""

    if len(a) != len(b):
        return False

    a = a.copy()
    b = b.copy()
    for concept_a in a:
        del_index = -1        
        for index, concept_b in enumerate(b):
            if concept_a.extent == concept_b.extent:
                del_index = index

        if del_index == -1:
            return False 

        del b[del_index]

    return True

class Comparator:

    def __init__(self, sign, func):
        self.sign = sign
        self.compare= func