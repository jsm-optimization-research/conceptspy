def CoveringRelation(cs):
    """Computes covering relation for a given concept system.
    
    Returns a dictionary containing sets of parents for each concept.
    """
    parents = dict([(c, set()) for c in cs])

    for i in range(len(cs)):
        for j in range(len(cs)):
            if cs[i].intent < cs[j].intent:
                parents[cs[j]].add(cs[i])
                for k in range(len(cs)):
                    if cs[i].intent < cs[k].intent and\
                       cs[k].intent < cs[j].intent:
                            parents[cs[j]].remove(cs[i])
                            break
    return parents