
from fca.algorithms.builders.norris import Norris
from fca import Context, Concept
from copy import deepcopy
import logging

logger = logging.getLogger("HYPOTHESES_BUILDER")
logger.setLevel(logging.DEBUG)

def BuildHypotheses(allPositiveConcepts, allNegativeConcepts):
    """Build positive hypotheses"""

    allPositiveConcepts = list(filter(lambda x: len(x.extent) >= 2, allPositiveConcepts ))
    allNegativeConcepts = list(filter(lambda x: len(x.extent) > 0, allNegativeConcepts ))
    
    positiveHypotheses = []
    for i, positiveConcept in enumerate(allPositiveConcepts):
        for j, negativeConcept in enumerate(allNegativeConcepts):
            logger.debug("{} / {} - {} / {}".format(i, len(allPositiveConcepts), j, len(allNegativeConcepts)))
            if positiveConcept.intent.issubset(negativeConcept.intent):
                break
        logger.debug("Add Hypothes")
        positiveHypotheses.append(positiveConcept)
        
    return positiveHypotheses

def WeighAnObject(hypotheses, intent):
    """Compute weigh for object by given hypotheses"""

    weigh = 0
    for hypothese in hypotheses:
        if hypothese.intent.issubset(intent):
            weigh += len(hypothese.extent)

    return weigh

def JSMClassifier(positiveContext, negativeContext, undefinedContext):
    """Classification on JSM-method of hypothesis generation 
    
    The JSM-method proposed by Viktor K. Finn in late 1970s was proposed as attempt to describe induction in purely
    deductive form and thus to give at least partial justification of induction"""

    # Support immutable
    positiveContext = deepcopy(positiveContext)
    negativeContext = deepcopy(negativeContext)
    undefinedContext = deepcopy(undefinedContext)

    # Build all (includes top and bottom concepts) 
    # of Concepts in Context (reason for having positive or negative class) 
    allPositiveConcepts = Norris(positiveContext)[0]
    allNegativeConcepts = Norris(negativeContext)[0]

    # Build hypotheses
    positiveHypotheses = BuildHypotheses(allPositiveConcepts, allNegativeConcepts)
    negativeHypotheses = BuildHypotheses(allNegativeConcepts, allPositiveConcepts)

    # Classification
    newUndefinedContext = Context([], undefinedContext.attributes, [])
    for index, intent in enumerate(undefinedContext.intents()):
        # Compute weigh of current undefined object with intent
        positiveWeigh = WeighAnObject(positiveHypotheses, intent)
        negativeWeigh = WeighAnObject(negativeHypotheses, intent)

        # If weighs eq add to new undefined context
        if positiveWeigh == negativeWeigh:
            newUndefinedContext.add_object(undefinedContext.objects[index], undefinedContext[index])
            continue            
        
        # If pos weigh greater add to positive context
        if positiveWeigh > negativeWeigh:
            positiveContext.add_object(undefinedContext.objects[index], undefinedContext[index])
        # Else to negative
        else:
            negativeContext.add_object(undefinedContext.objects[index], undefinedContext[index])
    
    return positiveContext, negativeContext, newUndefinedContext
