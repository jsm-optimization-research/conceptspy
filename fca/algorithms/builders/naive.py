from fca.concept import Concept
from fca.algorithms.utils import powerset

def Naive(context):
    """"Every formal concept of a context (G (objects); M (atrributes); I (binary relation)) has the form (X'';X')
    for some subset X of G and the form (Y'; Y'') for some subset Y of M. Vice
    versa all such pairs of sets are formal concepts.
    
    Additional information: https://www.hse.ru/data/2013/02/20/1306839179/Comparing%20performance%20of%20algorithms%20for%20generating%20concept.pdf
    """
    
    # Build list of set of attributes for every object in context
    intents = []
    for intent in context.intents():
        intents.append(intent)

    # Build list of set of objects for every attributes in context
    extents = []
    for extent in context.extents():
        extents.append(extent)
    
    # Set of all concepts
    L = dict()
    # Set of all objects
    G = set(context.objects)
    # Set of all attributes
    M = set(context.attributes)

    for objs_indexs in powerset(range(len(context))):
        # Сompute X'
        attrs = M.intersection(*[intents[i] for i in objs_indexs])

        # Compute X''
        attrs_indexs = list(map(lambda x: context.attributes.index(x), list(attrs)))
        objs = G.intersection(*[extents[i] for i in attrs_indexs])

        # Add concept (X'', X') to list of all concepts
        L[str(sorted(list(objs)))] = Concept(objs, attrs)
    
    # Remove duplicates concepts and return list of all concepts
    return ([v for k,v in L.items()], None)
        


