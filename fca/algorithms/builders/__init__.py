from .norris import Norris
from .naive import Naive
from .next_closure import NextClosure
from .cbo import CloseByOne