from fca import Concept


def NextClosure(context):
    """Next Closure algorithm for creating lattice from a formal concept. 
    Additional information: https://www.hse.ru/data/2013/02/20/1306839179/Comparing%20performance%20of%20algorithms%20for%20generating%20concept.pdf
    """
    
    # Build list of set of attributes for every object in context
    intents = []
    for intent in context.intents():
        intents.append(intent)

    # Build list of set of objects for every attributes in context
    extents = []
    for extent in context.extents():
        extents.append(extent)
    
    # Set of all concepts
    L = list()
    # Set of all objects
    G = set(context.objects)
    # Set of all attributes
    M = set(context.attributes)
    # Initional set for NextClosure algorithm
    A = set()
    # Max of objects
    g = max(context.objects)

    # Add bottom concept
    L.append(Concept(A, M))

    while A != G:
        # Compute next set to be examined
        A = A.union(set([g])).difference(set([h for h in A if g < h]))

        # Compute A'
        objs_indexs = list(map(lambda x: context.objects.index(x), list(A))) 
        attrs = M.intersection(*[intents[i] for i in objs_indexs])

        # Compute A''
        attrs_indexs = list(map(lambda x: context.attributes.index(x), list(attrs)))
        objs = G.intersection(*[extents[i] for i in attrs_indexs])

        # Canonical test A''
        if len(set([h for h in objs.difference(*A) if h < g])) == 0:
            # Add to set of all concepts
            L.append(Concept(objs, attrs))

            # End point
            if G == objs:
                break          
            
            # Update for next iteration
            g = max(G.difference(*objs))
            A = objs
        else:
            # Update for next iteration
            g = max(set([h for h in G.difference(*A) if h < g]))

    return (L, None)





