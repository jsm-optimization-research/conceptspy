from fca import Concept

def CloseByOne(context):
    """The algorithm generates concepts according to the lectic (lexicographic) order 
    on the subsets of G (concepts whose extents are lectically less are generated first).
    
    Additional information: https://www.hse.ru/data/2013/02/20/1306839179/Comparing%20performance%20of%20algorithms%20for%20generating%20concept.pdf
    """
    # Build list of set of attributes for every object in context
    intents = []
    for intent in context.intents():
        intents.append(intent)

    # Build list of set of objects for every attributes in context
    extents = []
    for extent in context.extents():
        extents.append(extent)
    
    # Set of all concepts
    L = dict()
    # Parents
    # P = dict()
    # Set of all objects
    G = set(context.objects)
    # Set of all attributes
    M = set(context.attributes)

    def Process(A, g, concept):
        if len([h for h in concept.extent.difference(A) if h < g]) == 0:
            L[str(sorted(list(concept.extent)))] = concept
            for f in G:
                if g < f:
                    Z = concept.extent.union(set(f))

                    # Compute f'
                    objs_indexs = list(map(lambda x: context.objects.index(x), [f])) 
                    attrs = M.intersection(*[intents[i] for i in objs_indexs])
                    Y = concept.intent.intersection(attrs)

                    # Compute Y'
                    attrs_indexs = list(map(lambda x: context.attributes.index(x), list(Y)))
                    X = G.intersection(*[extents[i] for i in attrs_indexs])

                    Process(Z, f, Concept(X, Y))

    # Start from root (Bottom concept)
    Process(set(), "", Concept(set(), M))

    # for g in sorted(list(G)):
    #     # Compute A'
    #     objs_indexs = list(map(lambda x: context.objects.index(x), [g])) 
    #     attrs = M.intersection(*[intents[i] for i in objs_indexs])

    #     # Compute A''
    #     attrs_indexs = list(map(lambda x: context.attributes.index(x), list(attrs)))
    #     objs = G.intersection(*[extents[i] for i in attrs_indexs])

    #     Process(set(g), g, Concept(objs, attrs))

    return ([v for k, v in L.items()], None)


