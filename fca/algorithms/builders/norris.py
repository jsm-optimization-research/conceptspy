from fca import Concept
import logging

logger = logging.getLogger("NORRIS BUILDER")
logger.setLevel(logging.INFO)

def Norris(context):
    """Build all concepts of a context

    Based on the Norris' algorithm for computing the maximal rectangles in a binary relation.
    https://www.researchgate.net/publication/243739216_An_algorithm_for_computing_the_maximal_rectangles_in_a_binary_relation

    Return list of all concepts of a context
    """

    logger.debug("start get all intents")
    # Build list of set of attributes for every object in context
    intents = []
    for intent in context.intents():
        intents.append(intent)
    logger.debug("finish get all intents")

    # Init set of concepts in context 
    L = [Concept([], context.attributes)]

    logger.info("start of building of concepts")
    # Build all concepts
    for i in range(len(context)):

        # List of concepts before processing current object
        prev_L = L[:]
        logger.info("Step: {} / {}".format(i, len(context)))
        
        for c in prev_L:
            if c.intent.issubset(intents[i]):
                logger.debug("Add extent: {}".format(context.objects[i]))
                # Concept update rule
                c.extent.add(context.objects[i])
            else:
                # Create new intent
                new_intent = c.intent & intents[i]
                new = True

                logger.debug("Start check new concept")

                # Check that new concept belongs T'
                for j in range(i):
                    logger.debug("Checking: {}".format(j))
                    if new_intent.issubset(intents[j]) and context.objects[j] not in c.extent:
                        new = False
                        break
                
                # Concept add rule
                if new:
                    new_concept = Concept(
                        list(set([context.objects[i]]) | c.extent),
                        new_intent
                    )
                    logger.debug("Add concept")
                    L.append(new_concept)
    
    return (L, None)