from fca.algorithms.utils import Comparator
from fca.algorithms.scaling import Scaling

def NominalScaling(attribute, mvcontext):
    """Return One-Valued context for the attribute of many-valued context by using nominal.
    
    This type of scaling is suitable for binary representation of nominal (categorical) attributes like color, gender and etc.
    """

    c = Comparator("==", lambda x, y: x == y)
    return Scaling(attribute, mvcontext, c)

def ContraNominalScaling(attribute, mvcontext):
    """Return One-Valued context for the attribute of many-valued context by using contranominal.
    
    """

    c = Comparator("!=", lambda x, y: x != y)
    return Scaling(attribute, mvcontext, c)