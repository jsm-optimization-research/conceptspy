from fca.algorithms.utils import Comparator
from fca.algorithms.scaling import Scaling

def OrdinalLEqScaling(attribute, mvcontext, values = []):
    """Return One-Valued context for the attribute of many-valued context by using ordinal less or equal.

    This type of scale can be used as an alternative for ordinal scaling like in example age, height and etc.
    """

    c = Comparator("<=", lambda x, y: x <= y)
    return Scaling(attribute, mvcontext, c, values)

def OrdinalGEqScaling(attribute, mvcontext, values = []):
    """Return One-Valued context for the attribute of many-valued context by using ordinal greate or equal.

    This type of scale can be used as an alternative for ordinal scaling like in example age, height and etc.
    """

    c = Comparator(">=", lambda x, y: x >= y)
    return Scaling(attribute, mvcontext, c, values)

def InterOrdinalScaling(attribute, mvcontext, values = []):
    """Return One-Valued context for the attribute of many-valued context by using interordinal.

    This type of scale can be used as an alternative for ordinal scaling like in example age, height and etc.
    """

    c = OrdinalLEqScaling(attribute, mvcontext, values)
    tmp_c = OrdinalGEqScaling(attribute, mvcontext, values)

    for index, attr in enumerate(tmp_c.attributes):
        values = [tmp_c[i][index] for i in range(len(tmp_c))]
        c.add_attribute(attr, values)

    return c

def IntervalScaling(attribute, mvcontext, values):

    c = Comparator(" - ", lambda x, y: y[0] <= x <= y[1])
    return Scaling(attribute, mvcontext, c, values)
    
