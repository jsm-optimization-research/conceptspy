from fca import Context

def Scaling(attribute, mvcontext, comparator, values = []):

    index = mvcontext.attributes.index(attribute)

    if index == -1:
        raise ValueError("Attribute {0} doesn't exist in context".format(attribute)) 

    if len(values) == 0:
        values = list(set([mvcontext[i][index] for i in range(len(mvcontext))])) 

    objs = mvcontext.objects.copy()
    attrs = ["{0} {1} {2}".format(attribute, comparator.sign, v) for v in values]
    table = [[mvcontext[i][index] if mvcontext[i][index] is None else comparator.compare(mvcontext[i][index], v) for v in values] for i, _ in enumerate(mvcontext.objects)]

    return Context(objs, attrs, table)