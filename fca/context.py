import copy
import texttable as tt
# from fuzz import FuzzyElement, FuzzySet


class Context:
    """A Formal Context consists of two sets: *objects* and *attributes*
    and of a binary relation between them."""

    def __init__(
            self, 
            objects,
            attributes,
            table
        ):
        """Create a context from cross table and list of objects, list of attributes"""
        
        if len(table) != len(objects):
            raise ValueError("Number of objects (={0}) and number of cross table rows(={1}) must agree"
                            .format(len(objects), len(table)))

        if (len(table) != 0) and len(table[0]) != len(attributes):
            raise ValueError("Number of attributes (={0}) and number of cross table columns (={1}) must agree"
                            .format(len(attributes), len(table[0])))

        self._table = table
        self._objects = objects
        self._attributes = attributes
    
    def get_objects(self):
        return self._objects

    objects = property(get_objects)

    def get_attributes(self):
        return self._attributes

    attributes = property(get_attributes)

    def intents(self):
        """Generator. Generate set of corresponding attributes
        for each row (object) of context
        """
        table = self._table
        label = self.attributes
        for item in table:
            label_indexes = filter(lambda i: item[i], range(len(item)))
            yield set([label[i] for i in label_indexes])

    def extents(self):
        """Generator. Generate set of corresponding objects
        for each column (attr) of context
        """
        table = self._transpose_table(self._table)
        label = self.objects
        for item in table:
            label_indexes = filter(lambda i: item[i], range(len(item)))
            yield set([label[i] for i in label_indexes])

    def add_attribute(self, attr_name, col):
        """Add new attribute to context with given name"""
        for i in range(len(self._objects)):
            self._table[i].append(col[i])
        self._attributes.append(attr_name)

    def add_object(self, obj_name, row):
        """Add new object to context with given name"""
        self._table.append(row)
        self._objects.append(obj_name)

    def derivation_attributes(self, concept):
        """Derivation on attributes"""
        objects = set()
        for index, intent in enumerate(self.intents()):
            if set(concept.intent).issubset(intent):
                objects.add(self.objects[index])
        
        return objects 

    def derivation_objects(self, concept):
        """Derivation on objects"""
        attributes = set()
        for index, extent in enumerate(self.extents()):
            if set(concept.extent).issubset(extent):
                attributes.add(self.objects[index])
        
        return attributes 

    def dichotomize(self):
        return Context(
            self._objects[:],
            self._attributes[:] + list(map(lambda x: "!" + str(x), self._attributes[:])),
            list(map(lambda x: x + list(map(lambda y: not bool(y), x)), self._table[:]))
        )
    
    def extract_subcontext_by_attributes(self, attrs):
        return Context(self.objects, attrs, self._extract_subtable_by_attributes(attrs))

    def extract_subcontext_by_objects(self, objs):
        return Context(objs, self.attributes, self._extract_subtable_by_objects(objs))

    def expansion_by_attribute(self, ctx):
        _res = copy.deepcopy(self)
        _ctx = ctx.extract_subcontext_by_objects(self.objects).extract_subcontext_by_attributes(set(ctx.attributes).difference(set(self.attributes)))
        for i, attr in enumerate(_ctx.attributes):
            col = []
            for obj in _ctx:
                col.append(obj[i])
            _res.add_attribute(attr, col)
        return _res
    
    def expansion_by_object(self, ctx):
        _res = copy.deepcopy(self)
        _ctx = ctx.extract_subcontext_by_attributes(self.attributes).extract_subcontext_by_objects(set(ctx.objects).difference(set(self.objects)))
        for i, obj in enumerate(_ctx.objects):
            _res.add_object(obj, _ctx[i])
        return _res

    def _extract_subtable_by_attributes(self, attributes):
        attribute_indices = [self.attributes.index(a) for a in attributes] 
        table = []
        for i in range(len(self)):
            row = []
            for j in attribute_indices:
                row.append(self[i][j])
            table.append(row)
        return table

    def _extract_subtable_by_objects(self, objs):
        object_indecs = [self.objects.index(o) for o in objs]
        return [self[i] for i in object_indecs]

    def __deepcopy__(self, memo):
        return Context(self._objects[:], self._attributes[:], copy.deepcopy(self._table, memo))
    
    def __len__(self):
        return len(self._table)

    def __getitem__(self, key):
        return self._table[key]

    def __repr__(self):
        tab = tt.Texttable()

        headings = [''] + self._attributes
        tab.header(headings)

        for index, row in enumerate(self._table):
            tab.add_row([self._objects[index]] + list(map(lambda x: "X" if x else ".", row)))
        
        output = tab.draw()
        return output

    def _transpose_table(self, table):
        return [[table[j][i] for j in range(len(table))] for i in range(len(table[0]))]
                       
    def transpose(self):
        """Return new context with transposed cross-table"""
        new_objects = self._attributes[:]
        new_attributes = self._objects[:]
        return Context(new_objects, new_attributes, self._transpose_table(self._table))