from .algorithms.builders import Norris 
from .algorithms import CoveringRelation

class ConceptLattice:
    """ConceptLattice class"""

    def __init__(self, context, builder=Norris):
        (self._concepts, self._parents) = builder(context)
        if self._parents is None:
            self._parents = CoveringRelation(self._concepts)

    def get_concepts(self):
        return self._concepts
    
    concepts = property(get_concepts)
    
    def __repr__(self):
        return str(self._concepts)

    def index(self, concept):
        return self._concepts.index(concept)
    
    def parents(self, concept):
        return self._parents[concept]

    def children(self, concept):
        return set([c for c in self._concepts if concept in self.parents(c)])