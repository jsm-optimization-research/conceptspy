import copy
import texttable as tt

class ManyValuedContext:
    """A Formal Context consists of two sets: *objects* and *attributes*
    and of a binary relation between them."""

    def __init__(
            self, 
            objects,
            attributes,
            table
        ):
        """Create a context from cross table and list of objects, list of attributes"""
        
        if len(table) != len(objects):
            raise ValueError("Number of objects (={0}) and number of cross table rows(={1}) must agree"
                            .format(len(objects), len(table)))

        if (len(table) != 0) and len(table[0]) != len(attributes):
            raise ValueError("Number of attributes (={0}) and number of cross table columns (={1}) must agree"
                            .format(len(attributes), len(table[0])))

        self._table = table
        self._objects = objects
        self._attributes = attributes
    
    def get_objects(self):
        return self._objects

    objects = property(get_objects)

    def get_attributes(self):
        return self._attributes

    attributes = property(get_attributes)

    def __deepcopy__(self, memo):
        return ManyValuedContext(self._objects[:], self._attributes[:], copy.deepcopy(self._table, memo))
    
    def __len__(self):
        return len(self._table)

    def __getitem__(self, key):
        return self._table[key]

    def __repr__(self):
        tab = tt.Texttable()

        headings = [''] + self._attributes
        tab.header(headings)

        for index, row in enumerate(self._table):
            tab.add_row([self._objects[index]] + list(map(lambda x: x, row)))
        
        output = tab.draw()
        return output
                       