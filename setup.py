import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="FcaPy",# Replace with your own username
    version="0.0.1",
    author="Rakipov Dinaf",
    author_email="rakipov69@gmail.com",
    description="FcaPy is a library for FCA (Formal Analyse Concepts), Context, Concept, Concept Lattice, Concept Lattice builders, implement of JSM method and etc.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vkr-2021/fcapy",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    keywords= "fca concept context lattice jsm classifier"
)
