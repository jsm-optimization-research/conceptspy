from fca import Concept
from fca.algorithms.utils import equalArrayOfConcepts
import unittest

class TestUtilsFunctions(unittest.TestCase):

    def setUp(self):
        self.testConceptSystem = [
            Concept([], ['a','b','c','d']),
            Concept(['1','2','3','4'], []),
            Concept(['1'], ['a','d']),
            Concept(['2'], ['a','c']),
            Concept(['3','4'], ['b','c']),
            Concept(['4'], ['b','c','d']),
            Concept(['1','2'], ['a']),
            Concept(['1','4'], ['d']),
            Concept(['2','3','4'], ['c']),
        ]
        self.testConceptSystem2 = self.testConceptSystem.copy()
        self.testConceptSystem2.append(Concept(['1','3'], ['a','b','d']))
        self.testConceptSystem3 = self.testConceptSystem.copy()
        del self.testConceptSystem3[-1]
        self.testConceptSystem3.append(Concept(['1','3'], ['a','b','d']))

    def test_equalArrayOfConcepts(self):
        self.assertEqual(equalArrayOfConcepts(self.testConceptSystem, self.testConceptSystem), True)
        self.assertEqual(equalArrayOfConcepts(self.testConceptSystem, []), False)
        self.assertEqual(equalArrayOfConcepts([], self.testConceptSystem), False)
        self.assertEqual(equalArrayOfConcepts(self.testConceptSystem, self.testConceptSystem3), False)
        self.assertEqual(equalArrayOfConcepts(self.testConceptSystem3, self.testConceptSystem), False)


if __name__ == "__main__":
    unittest.main()