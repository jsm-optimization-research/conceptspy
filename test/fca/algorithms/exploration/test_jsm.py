from fca import Context
from fca.algorithms.exploration import JSMClassifier
import unittest

def figures():
    attrs = ['c' + str(i) for i in range(1, 10)]

    posTable = [
        [1,1,1,1,0,0,1,0,0],
        [0,1,0,1,0,1,0,1,0],
        [0,1,1,0,0,1,1,0,1],
    ]

    negTable = [
        [1,1,1,0,1,0,0,1,1],
        [1,0,0,0,1,1,0,1,1],
        [0,1,1,0,0,1,0,1,1],
    ]

    undTable = [
        [1,1,0,1,1,0,1,0,0],
    ]

    posCtx = Context(['o1','o5','o7'], attrs, posTable)
    negCtx = Context(['o3','o4','o6'], attrs, negTable)
    undCtx = Context(['o2'], attrs, undTable)

    return posCtx, negCtx, undCtx

class TestJSMClassifier(unittest.TestCase):

    def setUp(self):
        self.posCtx, self.negCtx, self.undCtx = figures()

    def test_classifier_jsm(self):
        posCtx, negCtx, undCtx = JSMClassifier(self.posCtx, self.negCtx, self.undCtx)
        self.assertSetEqual(set(posCtx.objects), set(['o1','o5','o7', 'o2']))
        self.assertSetEqual(set(negCtx.objects), set(['o3','o4','o6']))
        self.assertSetEqual(set(undCtx.objects), set([]))



if __name__ == "__main__":
    unittest.main()