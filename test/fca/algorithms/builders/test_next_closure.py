from fca.algorithms.utils import equalArrayOfConcepts
from fca import Context, Concept
from fca.algorithms.builders import NextClosure
import unittest

class TestNextClosureBuilder(unittest.TestCase):

    def setUp(self):
        objs = ['1', '2', '3', '4']
        attrs = ['a', 'b', 'c', 'd' ]
        ct = [
            [1, 0, 0, 1],
            [1, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 1, 1, 1]
        ]
        ct = list(map(lambda x: list(map(lambda x: bool(x), x)), ct))

        self.ctx = Context(
            objs,
            attrs,
            ct
        )

        self.testConceptSystem = [
            Concept([], ['a','b','c','d']),
            Concept(['1','2','3','4'], []),
            Concept(['1'], ['a','d']),
            Concept(['2'], ['a','c']),
            Concept(['3','4'], ['b','c']),
            Concept(['4'], ['b','c','d']),
            Concept(['1','2'], ['a']),
            Concept(['1','4'], ['d']),
            Concept(['2','3','4'], ['c']),
        ]

    def test_builder(self):
        self.assertEqual(equalArrayOfConcepts(NextClosure(self.ctx)[0], self.testConceptSystem), True)


if __name__ == "__main__":
    unittest.main()