from fca import Concept
import unittest

class TestConcept(unittest.TestCase):

    def setUp(self):
        self.extent = ['Earth', 'Mars', 'Mercury', 'Venus'] 
        self.intent = ['Small size', 'Near to the sun'] 

    def test_contain_extent(self):
        c = Concept(self.extent, self.intent)
        self.assertEqual('Earth' in c.extent, True, "Should be True")
        self.assertEqual('Pluto' in c.extent, False, "Should be False")

    def test_contain_intent(self):
        c = Concept(self.extent, self.intent)
        self.assertEqual('Small size' in c.intent, True, "Should be True")
        self.assertEqual('Near to the moon' in c.intent, False, "Should be False")

if __name__ == "__main__":
    unittest.main()