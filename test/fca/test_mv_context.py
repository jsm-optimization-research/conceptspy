import copy
from fca import ManyValuedContext
import unittest

class TestManyValuedContext(unittest.TestCase):

    def setUp(self):
        self.objs = ['1', '2', '3', '4']
        self.attrs = ['a', 'b', 'c', 'd']
        self.ct = [
            [2, 1, 2, 3],
            [1, 2, 3, 1],
            [2, 1, 3, 1],
            [2, 3, 3, 2]
        ]
        self.c = ManyValuedContext(self.objs, self.attrs, self.ct)

    def test_emulate_container(self):
        self.assertEqual(len(self.c) == 4, True)
        self.assertEqual(self.c[0][0], 2)
        self.assertEqual(self.c[0][1], 1)

    def test_contain_obj(self):
        self.assertEqual('1' in self.c.objects, True)
        self.assertEqual('o1' in self.c.objects, False)

    def test_contain_attr(self):
        self.assertEqual('a' in self.c.attributes, True)
        self.assertEqual('z' in self.c.objects, False)


if __name__ == "__main__":
    unittest.main()